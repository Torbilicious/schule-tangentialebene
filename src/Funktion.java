import static java.lang.Math.pow;
import static java.lang.Math.sqrt;

public class Funktion
{
	@SuppressWarnings("unused")
	private double x, y;

	public double getFunction (double x, double y)
	{
		double z = Math.exp(sqrt((pow(x, 2) + pow(y, 2))));
		return z;
	}

	// grid version of the function
	public double[][] getGridFunction (double[] x, double[] y)
	{
		double[][] z = new double[y.length][x.length];
		for (int i = 0; i < x.length; i++)
		{
			for (int j = 0; j < y.length; j++)
			{
				z[j][i] = getFunction(x[i], y[j]);
			}
		}
		return z;
	}
}
