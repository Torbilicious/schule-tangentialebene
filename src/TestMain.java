public class TestMain
{
	public static void main (String[] args)
	{
		StartupData data = new StartupData();
		
		Funktion funktion = new Funktion ();
		
		TangentialebenFunktion surfaceCalculator = new TangentialebenFunktion (data);
		
		@SuppressWarnings("unused")
		Plotter plot = new Plotter (funktion, surfaceCalculator); //Optional noch mit neuer Funktion
	}
}
