import java.awt.Color;
import java.awt.KeyEventDispatcher;
import java.awt.KeyboardFocusManager;
import java.awt.event.KeyEvent;

import javax.swing.JFrame;

import org.math.plot.*;

import static org.math.array.DoubleArray.*;

class Plotter
{
	static StartupData tmpData;
	static Plot3DPanel plot;
	static TangentialebenFunktion surfaceCalculator;

	static double currentX = 0;
	static double currentY = 0;

	static double[] x2;
	static double[] y2;
	static double[][] z2;

	public Plotter (Funktion funktion, TangentialebenFunktion surfaceCalculator)
	{
		Plotter.surfaceCalculator = surfaceCalculator;
		
		// define your data1
		double[] x = increment(-5.0, 0.5, 5.0); // x = 0.0:0.1:1.0
		double[] y = increment(-5.0, 0.5, 5.0);// y = 0.0:0.05:1.0
		double[][] z1 = funktion.getGridFunction(x, y);

		// define your data2
		x2 = increment(-5.0, 1.0, 5.0); // x = 0.0:0.1:1.0
		y2 = increment(-5.0, 1.0, 5.0);// y = 0.0:0.05:1.0

		// create your PlotPanel (you can use it as a JPanel) with a legend at
		// SOUTH
		Plotter.plot = new Plot3DPanel("SOUTH");

		// add grid plot to the PlotPanel
		plot.addGridPlot("Funktion", x, y, z1);

		// put the PlotPanel in a JFrame like a JPanel
		JFrame frame = new JFrame("a plot panel");
		
		KeyboardFocusManager manager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
		manager.addKeyEventDispatcher(new MyDispatcher());
		
		frame.setSize(600, 600);
		frame.setContentPane(plot);
		frame.setVisible(true);
	}

	//Input Handling
	static class MyDispatcher implements KeyEventDispatcher
	{
		double stepSize = 0.1;

		@Override
		public boolean dispatchKeyEvent (KeyEvent e)
		{
			if (e.getID() == KeyEvent.KEY_PRESSED)
			{
				switch (e.getKeyCode())
				{
				case KeyEvent.VK_UP:
				{
					currentX += stepSize;
					break;
				}

				case KeyEvent.VK_DOWN:
				{
					currentX -= stepSize;
					break;
				}

				case KeyEvent.VK_LEFT:
				{
					currentY -= stepSize;
					break;
				}

				case KeyEvent.VK_RIGHT:
				{
					currentY += stepSize;
					break;
				}
				}
				
				double[] xValues = { currentX - 1.0, currentX, currentX + 1.0 };
				double[] yValues = { currentY - 1.0, currentY, currentY + 1.0 };
				
				System.out.println(currentX + ", " + currentY);
				
				try
				{
					plot.removePlot(1);
				}
				catch (Exception e2)
				{
				}
				
				plot.addGridPlot("Tangentialebene",
								 Color.RED,
								 xValues,
								 yValues,
								 surfaceCalculator.getGridFunction(currentX, currentY));
			}

			return false;
		}

	}
}
