public class TangentialebenFunktion
{
	@SuppressWarnings("unused")
	private double x, y, xNow, yNow;

	public TangentialebenFunktion (StartupData data)
	{
		this.x = StartupData.x;
		this.y = StartupData.y;
	}

	public double getFunction (double xNow, double yNow, double x0, double y0)
	{
		double wurzelTerm = Math.sqrt(Math.pow(x0, 2) + Math.pow(y0, 2));

		return ((x0 * (Math.pow(Math.E, wurzelTerm) / wurzelTerm) * (xNow - x0)) 
				+ (y0 * (Math.pow(Math.E, wurzelTerm) / wurzelTerm) * (yNow - y0))
				+ Math.pow(Math.E, wurzelTerm));
	}

	// grid version of the function
	public double[][] getGridFunction (double x0, double y0)
	{
		double[][] z = new double[3][3];
		double[] xValues = { x0 - 1.0, x0, x0 + 1.0 };
		double[] yValues = { y0 - 1.0, y0, y0 + 1.0 };

		for (int i = 0; i < 3; i++)
		{
			for (int j = 0; j < 3; j++)
			{
				z[j][i] = getFunction(xValues[i], yValues[j], x0, y0);
			}
		}
		return z;
	}
}
